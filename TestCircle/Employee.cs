﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCircle
{
    class Employee
    {
        private int id;
        private string firstName;
        private string lastName;
        private int salary;

        public Employee(int Id, string first, string last, int palkka)
        {
            id = Id;
            firstName = first;
            lastName = last;
            salary = palkka;
        }

        public int getID()
        {
            return id;
        }

        public string getFirstName()
        {
            return firstName;
        }

        public string getLastName()
        {
            return lastName;
        }

        public string getName()
        {
            return firstName + " " + lastName;
        }

        public int getSalary()
        {
            return salary;
        }

        public void setSalary(int palkka)
        {
            salary = palkka;
        }

        public int getAnnualSalary()
        {
            return salary * 12;
        }


    }
}
