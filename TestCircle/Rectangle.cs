﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCircle
{
    class Rectangle
    {
        private float length = 1.0f;
        private float width = 1.0f;

        public Rectangle ()
        {

        }

        public float getLength()
        {
            return length;
        }
        public void setLength(float l)
        {
            length = l;
        }

        public float getWidth()
        {
            return width;
        }
        public void setWidth(float l)
        {
            width = l;
        }

        public double  getArea()
        {
            return length * width;
        }

        public double getPerimeter()
        {
            return width * 2 + length * 2;
        }

    }
}
