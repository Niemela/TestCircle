﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCircle
{
    class InVoiceItem
    {
        private string id;
        private string desc;
        private int qty;
        private double unitPrice;

        public InVoiceItem(string ID, string Desc, int Qty, double UnitPrice)
        {
            id = ID;
            desc = Desc;
            qty = Qty;
            unitPrice = UnitPrice;
        }

        public string getID()
        {
            return id;
        }

        public int getQty()
        {
            return qty;
        }

        public void setQty(int qty)
        {

        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice(double UnitPrice)
        {
            
        }

        public double getTotal()
        {
            return unitPrice * qty;
        }
    }
}
