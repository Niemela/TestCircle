﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCircle
{
    class Account
    {
        private string id;
        private string name;
        private int balance = 0;

        public Account(string Id, string Name)
        {
            id = Id;
            name = Name;
        }
        public Account(string Id, string Name, int Balance)
        {
            id = Id;
            name = Name;
            balance = Balance;
        }

        public string getId()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }


    }
}